﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WSConvertisseur.Models
{
    public class Devise
    {
        public int Id { get; set; }

        public String NomDevise { get; set; }

        public double Taux { get; set; }


        public Devise()
        {

        }
    }
}
