﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WSConvertisseur.Models;

namespace WSConvertisseur.Controllers
{
    [Produces("application/json")]
    [Route("api/Devise")]
    public class DeviseController : Controller

    {
        private List<Devise> ListAll;

        public DeviseController()
        {

            ListAll = new List<Devise>();

            Devise d1 = new Devise();
            d1.Id = 1;
            d1.NomDevise = "Dollar";
            d1.Taux = 2;

            Devise d2 = new Devise();
            d2.Id = 2;
            d2.NomDevise = "Franc Suisse";
            d2.Taux = 1.07;

            Devise d3 = new Devise();
            d3.Id = 3;
            d3.NomDevise = "Yen";
            d3.Taux = 120;

            ListAll.Add(d1);
            ListAll.Add(d2);
            ListAll.Add(d3);
        }

        // GET: api/Devise
        [HttpGet]
        public IEnumerable<Devise> GetAll()
        {
            return this.ListAll;
        }

        // GET: api/Devise/5
        [HttpGet("{id}", Name = "GetDevise")]
        public IActionResult GetById(int id)
        {
            foreach (Devise devise in ListAll)
            {
                if (devise.Id == id) return Ok(devise);

            }
            return NotFound();
        }
        
        // POST: api/Devise
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Devise/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
